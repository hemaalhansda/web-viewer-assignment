function isLoggedIn() {
    const userInfo = JSON.parse(localStorage.getItem('isLoggedIn'));

    if (userInfo && userInfo.isLoggedIn) {
        return userInfo.isLoggedIn;
    }

    return false;
}

function logout() {
    localStorage.removeItem('isLoggedIn');
    return true;
}

module.exports = { isLoggedIn, logout };