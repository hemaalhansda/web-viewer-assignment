import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect
} from "react-router-dom";

import { createBrowserHistory } from 'history'

import Login from '../Components/Login/Login';
import Dashboard from '../Components/Dashboard/Dashboard';
import { isLoggedIn } from '../Services/AuthGaurd';

let history = createBrowserHistory();

export default class MainRouter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <Router history={history}>
                <Switch>
                    <Route exact path="/" render={
                        (props) => (isLoggedIn() ? <Dashboard {...props} /> : <Redirect to='/login' /> )
                    } />
                    <Route exact path="/login" component={Login} />
                </Switch>
            </Router>
        );
    }
}