import React from 'react';
import { logout } from '../../../Services/AuthGaurd';
import './Navbar.css';

import enterImage from './../../../Assets/Svgs/enter.svg';
import logoutIcon from './../../../Assets/Svgs/exit.svg';

export default class Navbar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            viewUrlOne: '',
            viewUrlTwo: ''
        };

        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    }

    componentDidMount() {
        let userInfo = JSON.parse(localStorage.getItem('isLoggedIn'));
        const width = window.innerWidth;
        this.setState({username: userInfo.username, width: width});
        window.addEventListener('resize', this.updateWindowDimensions);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    componentDidCatch(err, info) {
        console.log('this is error = ', err);
    }

    updateWindowDimensions() {
        this.setState({width: window.innerWidth, height: window.innerHeight}, () => {
            // console.log(`width = ${this.state.width}px`);
        });
    }

    logout = () => {
        if (logout()) this.props.history.push('/login');
    }

    handleInput = (event, type) => {
        this.setState({[type]: event.target.value});
    }

    render() {
        return (
            <React.Fragment>
                {
                    this.state.width >= 767 ?
                        <nav
                            className="navbar navbar-expand-lg navbar-light bg-light navbar-style"
                            style={{position: 'relative'}}>
                            <div className="row" style={{width: '100%'}}>
                                <div className="col-2">
                                    <a className="navbar-brand" href={'#mainPage'}>{this.state.username}</a>
                                </div>
                                <div className="col-4">
                                    <form className="url-form" target="iframe-view-1" method="GET" action={this.state.viewUrlOne}>
                                        <input
                                            onChange={(event) => this.handleInput(event, 'viewUrlOne')}
                                            className="form-control mr-sm-2 web-url" type="text" placeholder="Enter URL - 1" />
                                        {
                                            this.state.viewUrlOne && (<button className="submit-url-form" type="submit">
                                                <img className="enter-icon" src={enterImage} alt="Enter" />
                                            </button>)
                                        }
                                    </form>
                                </div>
                                <div className="col-4">
                                    <form className="url-form" target="iframe-view-2" method="GET" action={this.state.viewUrlTwo}>
                                        <input
                                            onChange={(event) => this.handleInput(event, 'viewUrlTwo')}
                                            className="form-control mr-sm-2 web-url" type="text" placeholder="Enter URL - 2" />
                                        {
                                            this.state.viewUrlTwo && (<button className="submit-url-form" type="submit">
                                                <img className="enter-icon" src={enterImage} alt="Enter" />
                                            </button>)
                                        }
                                    </form>
                                </div>
                                <div className="col-2"></div>
                            </div>
                            <button className="logout-button" onClick={this.logout}>
                                <img src={logoutIcon} className="logout-icon" alt="Logout" />
                                Logout
                            </button>
                        </nav> :
                        <nav
                            className="navbar navbar-expand-lg navbar-light bg-light navbar-style-sm"
                            style={{position: 'relative'}}>
                            <div className="row" style={{width: '100%'}}>
                                <div className="col-6">
                                    <a className="navbar-brand" href={'#mainPage'}>{this.state.username}</a>
                                </div>
                                <div className="col-6">
                                    <button className="logout-button-sm" onClick={this.logout}>
                                        <img src={logoutIcon} className="logout-icon" alt="Logout" />
                                        Logout
                                    </button>
                                </div>
                            </div>
                            <div className="row" style={{width: '100%'}}>
                                <div className="col-6">
                                    <form className="url-form" target="iframe-view-1" method="GET" action={this.state.viewUrlOne}>
                                        <input
                                            onChange={(event) => this.handleInput(event, 'viewUrlOne')}
                                            className="form-control mr-sm-2 web-url" type="text" placeholder="Enter URL - 1" />
                                        {
                                            this.state.viewUrlOne && (<button className="submit-url-form" type="submit">
                                                <img className="enter-icon" src={enterImage} alt="Enter" />
                                            </button>)
                                        }
                                    </form>
                                </div>
                                <div className="col-6">
                                    <form className="url-form" target="iframe-view-2" method="GET" action={this.state.viewUrlTwo}>
                                        <input
                                            onChange={(event) => this.handleInput(event, 'viewUrlTwo')}
                                            className="form-control mr-sm-2 web-url" type="text" placeholder="Enter URL - 2" />
                                        {
                                            this.state.viewUrlTwo && (<button className="submit-url-form" type="submit">
                                                <img className="enter-icon" src={enterImage} alt="Enter" />
                                            </button>)
                                        }
                                    </form>
                                </div>
                            </div>
                        </nav>
                }
            </React.Fragment>
        );
    }
}