import React from 'react';
import './Dashboard.css';

import Navbar from './Navbar/Navbar';
import MainSection from './MainSection/MainSection';

export default class Dashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div id="mainPage" className="custom-container">
                <Navbar history={this.props.history} />
                <MainSection />
            </div>
        );
    }
}