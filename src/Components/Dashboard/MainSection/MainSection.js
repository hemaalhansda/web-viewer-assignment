import React from 'react';
import './MainSection.css';

import Iframe from 'react-iframe';

export default class MainSection extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div className="row main-section-container">
                <div className="col-xl-6 col-md-6 col-sm-12 webpage-frame">
                    <Iframe
                        width="100%"
                        height="100%"
                        name="iframe-view-1"
                        scrolling="auto"
                        className="iframe-css"
                        display="initial"
                        position="relative" />
                </div>
                <div className="col-xl-6 col-md-6 col-sm-12 webpage-frame">
                    <Iframe
                        width="100%"
                        height="100%"
                        name="iframe-view-2"
                        scrolling="auto"
                        className="iframe-css"
                        display="initial"
                        position="relative" />
                </div>
            </div>
        );
    }
}