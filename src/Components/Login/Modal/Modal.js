import React from 'react';
import './Modal.css';

const Modal = (props) => {
    return (
        <React.Fragment>
            <div className="modal-container">
                <span className="reset-label">Reset your password!</span>
                <input
                    type="password"
                    className="form-control modal-pass"
                    onChange={(event) => props.handleInput(event, 'newPassword')}
                    placeholder="Your New Password *"
                    defaultValue="" />
                <button className="btnSubmit reset-button" onClick={props.resetPassword}>Reset</button>
            </div>
            <div className="overlay" onClick={() => props.modalHandler(false)}></div>
        </React.Fragment>
    );
}

export default Modal;