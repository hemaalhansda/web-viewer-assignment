import React from 'react';
import './Login.css';

import loginLeftLogo from './../../Assets/Svgs/intro_logo.svg';
import { isLoggedIn } from '../../Services/AuthGaurd';
import Modal from './Modal/Modal';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export default class Login extends React.Component {
    constructor(props, context) {
        super(props);
        this.state = {
            username: '',
            password: '',
            newPassword: '',
            error: '',
            modal: false
        };

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        if (isLoggedIn()) {
            this.props.history.push('/');
        }
    }

    inputEmptyCheck = () => {
        if (!this.state.username.length || !this.state.password.length) return false;
        return true;
    }

    handleSubmit = (event) => {
        event.preventDefault();
        if (!this.inputEmptyCheck()) {
            alert("Please fill up all the fields!");
            return;
        }
        let userList = JSON.parse(localStorage.getItem('userList'));
        if (userList && userList.length) {
            const userExist = userList.filter(user => user.username === this.state.username);
            
            if (userExist.length) {
                const verified = userList.filter(user => user.username === this.state.username
                    && user.password === this.state.password);

                if (!verified.length) {
                    this.setState({error: 'Incorrect credentials!'});
                    return;
                }
                
                localStorage.setItem('isLoggedIn', JSON.stringify({isLoggedIn: true, username: verified[0].username}));
                this.props.history.push('/');
            } else {
                this.createNewUser(userList);
                return;
            }
        }

        this.createNewUser([]);
    }

    createNewUser = (userList) => {
        const userInfo = {
            username: this.state.username,
            password: this.state.password
        };
    
        userList.push(userInfo);
        localStorage.setItem('userList', JSON.stringify(userList));
        localStorage.setItem('isLoggedIn', JSON.stringify({isLoggedIn: true, username: this.state.username}));
        this.props.history.push('/');
    }

    handleInput = (event, type) => {
        this.setState({[type]: event.target.value, error: ''});
    }

    checkUserExist = () => {
        let userList = JSON.parse(localStorage.getItem('userList'));
        userList = userList.filter(user => user.username === this.state.username);
        
        return userList.length ? true : false;
    }

    modalHandler = (boolVal) => {
        if (!this.checkUserExist()) {
            alert("User not exist.");
            return;
        }

        if (boolVal && !this.state.username.length) {
            alert("Please enter the username first.");
            return;
        }
        this.setState({modal: boolVal});
    }

    notify = (message) => toast(message);

    resetPassword = () => {
        if (!this.state.newPassword.length) {
            alert("The password field can't be empty.");
            return;
        }

        let userList = JSON.parse(localStorage.getItem('userList'));
        userList = userList.map(user => {
            if (user.username === this.state.username) {
                user.password = this.state.newPassword;
            }
            return user;
        });

        localStorage.setItem('userList', JSON.stringify(userList));
        this.notify("Password modified successfully!");
        this.modalHandler(false);
    }

    render() {
        const centerStyle = {
            display: 'flex',
            justifyContent: 'center'
        };
        return (
            <div className="main-container">
                {
                    this.state.modal && (
                        <Modal
                            resetPassword={this.resetPassword}
                            handleInput={this.handleInput}
                            modalHandler={this.modalHandler} />
                    )
                }
                <div className="back-image"></div>
                <div className="container login-container">
                    <div className="row">
                        <div className="col-md-6 login-form-1 banner" style={{backgroundColor: '#f6f6f6'}}>
                            <img src={loginLeftLogo} alt="Web Viewer Assignment" style={{width: '285px'}}/>
                        </div>
                        <div className="col-md-6 login-form-2">
                            <h3>LOGIN</h3>
                            <form
                                style={{
                                    position: 'relative'
                                }}
                                onSubmit={this.handleSubmit}>
                                <div className="form-group">
                                    <input
                                        type="text"
                                        className="form-control"
                                        onChange={(event) => this.handleInput(event, 'username')}
                                        placeholder="Your Username *"
                                        defaultValue="" />
                                </div>
                                <div className="form-group">
                                    <input
                                        type="password"
                                        className="form-control"
                                        onChange={(event) => this.handleInput(event, 'password')}
                                        placeholder="Your Password *"
                                        defaultValue="" />
                                </div>
                                <label style={{
                                    fontSize: '12px',
                                    position: 'absolute',
                                    top: '16px',
                                    color: 'red',
                                    fontWeight: 700
                                }}>{this.state.error}</label>
                                <div className="form-group" style={centerStyle}>
                                    <input type="submit" className="btnSubmit"/>
                                </div>
                                <div className="form-group" style={centerStyle}>
                                    <span className="ForgetPwd" onClick={() => this.modalHandler(true)}>Forget Password?</span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <ToastContainer autoClose={2000} />
            </div>
        );
    }
}