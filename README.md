# Web Viewer

Web Viewer is a web-application where user can visit the two webpages using url or html-link within the same page.

## Installation

``` bash
# clone the repository
$ git clone https://hemaalhansda@bitbucket.org/hemaalhansda/web-viewer-assignment.git

# go into app's directory
$ cd web-viewer-assignment

# install app's dependencies
$ npm install

# run the app
$ npm start
```

## Some Informations

- Login page
``` bash
1. User can be created only once.
2. User cannot modified their usernames.
3. If the user is created once, it cannot be removed.
4. User cannot login with different password once the user is created.
```

- Main page
``` bash
1. Cannot view the webpages which are "X-Frame" protected.
   Example - google, facebook, amazon, etc.
2. For testing, user can try entering this url "http://aziels.com", "http://javascript.info/", etc.
```

